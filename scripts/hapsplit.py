import pysam
import sys

def hapsplit(inBam,hap1,hap2, hapmisc):
    samfile = pysam.AlignmentFile(inBam, "rb")
    H1 = pysam.AlignmentFile(hap1, "wb", template=samfile)
    H2 = pysam.AlignmentFile(hap2, "wb", template=samfile)
    HM = pysam.AlignmentFile(hapmisc, "wb", template=samfile)
    for read in samfile.fetch():
        try:
            HP=read.get_tag('HP')
            if float(HP) == 1:
                H1.write(read)
            if float(HP) == 2:
                H2.write(read)
        except:
           HM.write(read)
    H1.close()
    H2.close()
    HM.close()
    samfile.close()

hapsplit(sys.argv[1],sys.argv[2],sys.argv[3], sys.argv[4])




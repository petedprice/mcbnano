# mcbnano

Analysis workflow for ONT data from mouse CRISPR amplicon sequencing 

## Dependencies

-nextflow

-albacore or guppy (optional)

-filtlong

-minimap2

-samtools

-nanopolish

-medaka

-whatshap

-bedtools

-bedops
  
-seqtk

-blast

-pysam

-pandas

-matplotlib

## Installation
Assuming all the dependencies are installed, the workflow can be installed by pulling from the gitlab page. There is also the option to run with conda using the -conda flag at runtime.

```
git clone https://gitlab.com/nick297/cas9point4.git
```

### Docker

### Singularity



## Run Strategy 

cas9point4 gives you two alternate strategies for investigating mouse CRISPR amplicons. 
Strategy is defined by the flag 

```
--strategy filt_by_determinants, medaka_whatshap
```


### Determinant filtering 
Filtering by determinants requires you to define an edited portion of the amplicon, i.e. the insertion of a LoxP or Cre site and detect this sequence within your reads. Reads that match a blast search of this defined sequence will be maintained and all other reads will be thrown out of the pileup. This strategy aims to report only those reads that represent correct or close-to correct mutants that contain your desired modifications. Variants are also called from the final filtered BAM file using medaka to identify small variants. 

### Variant call and phase 
The second strategy choice is to make no assumptions of the outcome of the edits. This path simply calls variants from all aligned reads using medaka, filters variants that lie within low complexity regions using seqtk and dustmasker, then phases reads, using whatshap, based on the final set of variants producing two final BAM files representing two distinct alleles. At this point, the pipeline is assuming that there is a maximum of two alleles so there may be loss of power within complex mosaics. Individual allele BAMs are once again variant called to produce independent VCFs.



## Run 
To run, a comma seperated (.csv) determinants file is required that lists the barcodes and reference sequences for mapping. Each barcode can be assigned multiple reference sequences for which it will be aligned to.

Here is an example of the csv file:

|  sample name                    | barcode | Number of determinants | name of determinant 1 | Length of determinant 1 | name of determinant 2 | Length of determinant 2 | name of determinant 3 | Length of determinant 3 | length % | Mutant ref name  |
|---------------------------------|---------|------------------------|-----------------------|-------------------------|-----------------------|-------------------------|-----------------------|-------------------------|----------|------------------|
|  MPEG1-CRE-CAS-LINE4-B6N/1.1c   | BC01    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  MPEG1-CRE-CAS-LINE3-B6N/1.1d   | BC02    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  PRO/4274.1h                    | BC03    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  PRO/4274.4a                    | BC04    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  CX3CL1-FLOX-CAS-LINE1-B6N/1.1c | BC05    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Cx3cl1\_Flox.fas  |
|  PRO/3976.1f                    | BC06    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Cx3cl1\_Flox.fas  |
|  PRO/4264.4a                    | BC07    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Pam\_Flox.fas     |
|  PAM-FLOX-CAS-LINE1-B6N/1.1a    | BC08    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Pam\_Flox.fas     |
|  PRO/4345.5a                    | BC09    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Prdm8\_Flox.fas   |
|  PRO/4345.3g                    | BC10    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Prdm8\_Flox.fas   |
|  PRO/4282.4e                    | BC11    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Hnf1a\_Flox.fas   |
|  PRO/4405.4a                    | BC12    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Inpp5k\_Flox.fas  |



This file can be called 'determinants_template.csv' and it will be detected automatically, or can be specified with the --determinantsFile option.

The location of the fast5 or fastq files is required with the --inFiles flag.

The location (directory of) of the reference sequence(s), correspoding to the determinants_file.csv meta data, must be specified with the --refDir option.

If choosing to filter by determinants the location (file) of the determinants sequences (multi fasta file), corresponding to the determinants_file.csv meta data, must be specified with the --determinantsFasta option.


If you have downloaded this git repo, you can run the workflow like such:


```
If you want to run 
# With basecalling (guppy (default) or albacore) and demultiplexing using the determinants based strategy
nextflow run /location/of/cas9point4/main.nf \
	--basecall true \
	--demultiplex true \
        --inFiles /location/of/fast5_files \
        --inExp exp3 \
        --refDir /location/of/soft/cas9point4/refs/ \
	--strategy filt_by_determinants \
        --determinantsFasta /location/of/cas9point4/transgene_sequences/loxp_cre.fa \
        -profile conda 

# With already basecalled fastq files but using the medaka and phasing approach 
nextflow run  /location/of/cas9point4/main.nf \
	--demultiplex true \
        --inFiles /location/of/fastq_files \
        --inExp exp3 \
        --refDir ~/cas9point4/refs/ \
	--strategy medaka_whatshap \
        -profile conda 

```


## Outputs
The workflow will output a number of folders containing files generated. These include:

#### Universal outputs:

'all': Bam files before filtering by determinants, containing all the reads remaining after filtering with filtlong


'basecalled': basecalled fastq (optional true if basecall=true)


'Bed_files': folder of bed files defining pooled low complexity regions identified by seqtk and dustmasker 


'covBases': plots of coverage proportion by majority base, insertions and deletions


'depthCharts': plots of coverage depth


'filtLong': fastq file output from filtong


'Demultiplexed_fastqs': folder containing demultiplexed fastqs. If demultiplexed reads will have prefix 'barcode' (optional true if demultiplex=true)


'psyamStats': output text file from pysamstats


'stats': stats txt files from samtools depths and idxstats

'nanoplot': visualisations and data about aligned read size distribution and qualities 


#### Optional determinant filtering outputs:

'passBams': bam files only containing reads that pass the determinants file criteria


'passBam_varcall': Variants called by medaka from BAMs after being filtered for determinant sequences 


'passFailPlots': plot of read depth coverage for reads that passed or failed the determinants file criteria


#### Optional variant calling and phasing outputs: 

'First_pass_varcall': Variants called by medaka. Includes folders containing complete output produced by medaka (e.g. medaka_BC02_Tra2b_Flox) and a VCF filtered by seqtk and dustmasker per sample (e.g  medaka_Tra2b_Flox_BC02_filt.vcf)


'haplotagged': Folder of BAM files where reads are tagged by whatshap according to the allele they have been assigned to by the variants from medaka (e.g exp2_BC02_Tra2b_Flox_q90.haplotagged.bam)


'hapsplit': Folder containing allele BAM files split by allele tags defined by whatshap. Contains a BAM for allele 1, allele 2, and misc (reads that did not receive a tag) (e.g exp2_BC02_Tra2b_Flox_q90.hap1.bam). Also contains folders of complete output from medaka after variants are called from allele 1,2 and misc. 


'Haplotyped_vcfs': Contains VCFs for allele 1 and allele 2 after filtering for low complexity regions identified by dustmasker and seqtk





## Flags

```

--inExp				State experiment name or number

--inFiles			Path to fast5 or fastqs

--refDir			Path to references 

--determinantsCsv		Path to determinants meta data file	

--determinantsFasta		Path to determinants fasta sequence file

--basecall			true or false (defaut=false)

--basecaller			Decide basecaller option: guppy_cpu, guppy_gpu, albacore (default=guppy_cpu)

--basecaller_model		Model used with guppy basecaller, options can be found under /opt/ont/guppy/data/ (default=dna_r9.4.1_450bps_hac.cfg)

--demultiplex          		true or false (default=false)

--require_barcodes_both_ends	Decide when demultiplexing if only reads with forward and reverse barcodes are maintained: true or false (default=true)

--barcode_kit			Barcoding kit during library prep. Must be enclosed with "" (default="EXP-PBC001")

--qual				Quality score used by filtlong (default=90)

--min_seq_length		Remove reads shorter than stated % of reference length. E.g stating '80' with a 1000nt reference will keep only reads 800nt or longer. Warning: if sample sits across multiple amplicons, you will likely loose all reads. (default=0, no reads are removed)

--nanoplot			Choose whether to produce nanoplot statistics from filtlong filtered fastqs or alligned BAM file (BAM will have a smaller subset of reads but will give statistics on sequence identity): bam, fastq, both (default=both)

--strategy			Strategy for subsetting reads using either the determinants based method or variant calling and phasing: filt_by_determinants, medaka_whatshap (default=filt_by_determinants)

--medaka_model			Model used by medaka for variant calling, dependent on pore and basecaller. Full model list can be found in medaka's documentation. (default=r941_min_high_g360))

--dustmasker			Option to filter variants called by medaka using dustmasker: true, false (default=true)

--homopol_length		Filter variant that sit in homopolymers of length N or greater (default=5)

```



## Resources 

[NanoPlot Github] (https://github.com/wdecoster/NanoPlot)


[Medaka Documentation](https://nanoporetech.github.io/medaka/walkthrough.html)


[Whatshap Documentation](https://whatshap.readthedocs.io/en/latest/#)


[Guppy Documentation]

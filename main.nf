params.base="$baseDir"
inExp = params.inExp
fast=Channel.fromPath(params.inFiles)
params.refDir="${params}.baseDir" + 'refs/'
params.determinantsCsv='determinants_template.csv'
determinants=file(params.determinantsCsv)
dFasta=file(params.determinantsFasta)
determinantsFasta=Channel.from( dFasta )

params.basecall=false
params.gpu=false
if (params.gpu == true) { 
	gpu_device = "--device cuda:0"
} else {
	gpu_device = ""
}

params.basecaller_model="dna_r9.4.1_450bps_hac.cfg"
bc_model=params.basecaller_model

params.demultiplex=false
params.require_barcodes_both_ends=true
if (params.require_barcodes_both_ends == true) {
	both_ends = "--require_barcodes_both_ends"
} else {
	both_ends = ""
}
params.barcode_kit='"EXP-PBC001"'
barcode_kit=params.barcode_kit
params.qual=90
params.nanoplot='both'
params.min_seq_length=0
msl=params.min_seq_length
quals= Channel.from( params.qual )

params.strategy='filt_by_determinants'
params.align_fraction = 0.725

params.vc_covs=2000
vc_covs=params.vc_covs
params.medaka_model="r941_min_high_g360"
med_model=params.medaka_model

params.dustmasker=true
params.homopol_length=5
hpl=params.homopol_length


Channel
        .from( determinants )
        .splitCsv()
        .map { row -> tuple(row[1],row[10]) }
        .view()
        .set{ metaData }


if (params.basecall == true) {
process basecall {
	tag { run }

	publishDir 'basecalled', mode: 'link', pattern: '*fastq*'
	cpus 4	

	input:
	//set val(run), val(fast5s) from ginputs1
        val(run) from inExp
	file fast5s from fast
	

	output:
	set val(run), file(fast5s), file(fastqs) into basecalled,basecalled2

	script:
	"""
	guppy_basecaller -i $fast5s -c /opt/ont/ont-guppy/data/${bc_model} -s fastqs -r --num_callers 4 ${gpu_device}
	"""

					
}}




if (params.demultiplex == true) {
if (params.basecall == true) {

process gup_bar {
	tag { run }

	publishDir 'Demultiplexed_fastqs'

	cpus 4

	input:
	set val(run), val(fast5s), file(fastqs) from basecalled

	output:
	file("guppy_barcoder_fastqs/barcode*") into demulti_adaptorchop

	
	script:
        """
        guppy_barcoder -i $fastqs -s guppy_barcoder_fastqs -t ${task.cpus} -r ${both_ends} --trim_barcodes --detect_mid_strand_barcodes --barcode_kit ${barcode_kit} ${gpu_device}
        """
}}

else if(params.basecall == false) {
process guppy_demultiplexnb {
	tag { run }

	publishDir 'Demultiplexed_fastqs'

	cpus 4

	input:
	val(run) from inExp
	val(fastqs) from fast
	val(be) from both_ends

	output:
	file("guppy_barcoder_fastqs/barcode*") into demulti_adaptorchop

	
	script:
	"""
	guppy_barcoder -i $fastqs -s guppy_barcoder_fastqs -t ${task.cpus} -r ${both_ends} --trim_barcodes --detect_mid_strand_barcodes --barcode_kit ${barcode_kit} ${gpu_device}
	"""

}
}}

if (params.demultiplex == true) {
	
	demultiplexed= demulti_adaptorchop.flatten()
} else {
	demultiplexed = Channel
			.fromPath( params.inFiles + '/*.fastq' )
			.flatten()
}


process filtLong {
        tag { bc + ' ' + q }
	//conda 'bioconda::filtlong'
	publishDir 'filtLong'
	cpus 1

	input:
	tuple file(fastq),val(q) from demultiplexed.combine(quals)

	output:	
	tuple val(bc), file("${bc}_q${q}.fastq.gz"),val(q) into filtLonged
	

	script:
	if (params.demultiplex == false)
		bc = fastq.baseName.replace(".fastq","")
	else if (params.demultiplex == true)
		bc=fastq.baseName.replace("barcode", "BC")
	

	if (params.demultiplex == false)
		"""		
		filtlong --min_mean_q $q  ${bc}.fastq | gzip > ${bc}_q${q}.fastq.gz
		"""	
	else if (params.demultiplex == true)
		"""		
		cat ${fastq}/*.fastq > ${bc}.fastq
		filtlong --min_mean_q $q  ${bc}.fastq | gzip > ${bc}_q${q}.fastq.gz
		"""

	
}



process makeBlastDB {
	input:
	file('determinants.fa') from determinantsFasta
	
	output:
	tuple file('determinants.fa'), file('determinants.fa.nhr'), file('determinants.fa.nin'), file('determinants.fa.nsq') into blastdb,blastdb2

	script:
	"""
	makeblastdb -in determinants.fa -dbtype nucl
	"""

}

process preRef {
	tag { bc + ' ' + d + ' ' + ref}
        //conda 'bioconda::blast'

        input:
        tuple val(bc), file("${bc}_${q}.fastq.gz"),val(q), val(ref), file('determinants.fa'), file('determinants.fa.nhr'), file('determinants.fa.nin'), file('determinants.fa.nsq') from filtLonged.combine(metaData, by:0).combine(blastdb)

        output:
        tuple val(bc), file("${bc}_${q}.fastq.gz"),val(q),val(ref),file("*.fas"),file("${ref}.blast") into refs

        script:
        d=params.refDir
        """
        echo
        cp $d/${ref} ./
        blastn -query ${ref} -db determinants.fa -outfmt 6 -out ${ref}.blast 
        """
}




process map {
        tag { bc + '_' + r}
        //conda 'bioconda::minimap2 bioconda::samtools bioconda::pysam'
	cpus 4

	publishDir 'all', mode: 'copy', overwrite: true, pattern: '*.bam*'

	input:
        tuple val(bc), file('fastq'),val(q),val(r),file('ref'),file("${r}.blast") from refs


	output:
        tuple val(bc), val(r), val(exp), file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"),file("ss.fastq"),file('ref'),file("${r}.blast"),val(q) into assembled1,assembled2
	tuple val(bc), val(r), val(exp), val(q), file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file("ss.fastq")  into nanoplt
	script:
        exp=params.inExp
	
	
	if (msl == 0)
		"""
		seqtk seq -L0 fastq > ss.fastq 
		minimap2 -ax map-ont $ref ss.fastq |\
        	samtools view -bS -q 50 - | samtools sort -o out.bam
        	samtools index out.bam
        	filterBam.py out.bam ${exp}_${bc}_${r}_q${q}.sorted.bam 0.50 0.3
        	samtools index ${exp}_${bc}_${r}_q${q}.sorted.bam
		"""

	else if (msl > 0) 
		"""	
		let min=\$(bioawk -c fastx '{print length(\$seq)}' $ref)*$msl/100
		seqtk seq -L\${min} fastq > ss.fastq
		minimap2 -ax map-ont $ref ss.fastq  |\
		samtools view -bS -q 50 - | samtools sort -o out.bam 

	        samtools index out.bam
      		filterBam.py out.bam ${exp}_${bc}_${r}_q${q}.sorted.bam 0.50 0.3
		samtools index ${exp}_${bc}_${r}_q${q}.sorted.bam
        	"""
}

process nanoplot {
	errorStrategy 'ignore'
	tag { bc + '_nanoplot_' + r}
        //conda 'bioconda::NanoPlot
        cpus 4

        publishDir 'nanoplot', mode: 'copy', overwrite: true, pattern: '*nanoplot*'


	input:
	tuple val(bc), val(r), val(exp), val(q),  file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"),  file("ss.fastq") from nanoplt 

	output:
	tuple val(bc), val(r), file("nanoplot_${bc}_${r}*") into nansave	

	script:
	if (params.nanoplot == 'bam')
		"""
		NanoPlot --bam ${exp}_${bc}_${r}_q${q}.sorted.bam -o nanoplot_${bc}_${r}_bam -p ${bc}_${r} --raw
		"""
	else if (params.nanoplot == 'fastq')
		"""
                NanoPlot --fastq ss.fastq -o nanoplot_${bc}_${r}_fastq -p ${bc}_${r} --raw
                """
 	else if (params.nanoplot == 'both')
                """
                NanoPlot --fastq ss.fastq -o nanoplot_${bc}_${r}_fastq -p ${bc}_${r} --raw
                NanoPlot --bam ${exp}_${bc}_${r}_q${q}.sorted.bam -o nanoplot_${bc}_${r}_bam -p ${bc}_${r} --raw
		"""

}

process bamStats {
	tag { bc + '_' + r}
        //conda 'bioconda::samtools pandas seaborn matplotlib pysamstats'

	publishDir 'stats', mode: 'copy', overwrite: true, pattern: '*.tsv'
        publishDir 'depthCharts', mode: 'copy', overwrite: true, pattern: '*depth.pdf'
        publishDir 'covBases', mode: 'copy', overwrite: true, pattern: '*covbases.pdf'
        publishDir 'psyamStats', mode: 'copy', overwrite: true, pattern: '*.pysam'
        

	input:
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q) from assembled1

	output:
        file("*idxstats.tsv") into bstats
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv") into depths
        file("*.pdf") into depthCharts
        file("*.pysam") into pysamStats

	script:
	"""
	samtools idxstats ${exp}_${bc}_${r}_q${q}.sorted.bam > ${exp}_${bc}_${r}_${q}.idxstats.tsv
        samtools depth -m 0 -aa ${exp}_${bc}_${r}_q${q}.sorted.bam > ${exp}_${bc}_${r}_${q}.depth.tsv
        plotDepth.py ${exp}_${bc}_${r}_${q}.depth.tsv
	pysamstats -t variation_strand -f ref -d ${exp}_${bc}_${r}_q${q}.sorted.bam -D 300000 > ${exp}_${bc}_${r}_${q}.pysam
        covBases.py ${exp}_${bc}_${r}_${q}.pysam ${exp}_${bc}_${r}_${q}

	"""
}



process vcf_filt {
	errorStrategy 'ignore'
	tag { bc + '_' + r }
	//conda 'bioconda:: seqtk bedops dustmasker bedtools'
	publishDir 'Bed_files' , mode: 'copy', overwrite: true, pattern: '*filt.bed'
	
	input: 
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv") from depths
	
	output:
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed") into bed_varcall

	
	script:
	hpl=params.homopol_length

	if (params.dustmasker == false)
		"""
		seqtk hrun ref ${hpl} > ${r}_seqtk.bed
		sort -k1,1 -k2,2n ${r}_seqtk.bed | cut -f1,2,3 > cat.sorted.bed
		bedops --range -1:1 --everything cat.sorted.bed > ${r}_${bc}_filt.bed
		"""
	else if (params.dustmasker == true)
		"""
		dustmasker -in ref -out dust.bed -level 14 -outfmt acclist
		cut -c 2- < dust.bed > rdust.bed
		bedops --range 0:1 --everything rdust.bed > ${r}_${bc}_dust.bed
		seqtk hrun ref ${hpl} > ${r}_seqtk.bed
		cat ${r}_seqtk.bed ${r}_${bc}_dust.bed 2>/dev/null | sort -k1,1 -k2,2n | cut -f1,2,3 > cat.sorted.bed
		bedops --range -1:1 --everything cat.sorted.bed > ${r}_${bc}_filt.bed
		"""

}




if (params.strategy == 'filt_by_determinants') {
process blastRawReads {
	tag { bc + '_' + r}
        //conda 'bioconda::blast'

	input:
	
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file('determinants.fa'), file('determinants.fa.nhr'), file('determinants.fa.nin'), file('determinants.fa.nsq') from bed_varcall.combine(blastdb2)

	output:
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file("${bc}.blast") into rblast

        script:
        """
	sed -n '1~4s/^@/>/p;2~4p' fastq > fasta
        blastn -query fasta -db determinants.fa -outfmt 6 -out ${bc}.blast 
        """

}


process filterByDeterminants {
        errorStrategy 'ignore'
	tag { bc + '_' + r }
        //conda 'bioconda::samtools pandas seaborn matplotlib pysamstats pysam'

        publishDir 'passFailPlots', mode: 'copy', overwrite: true, pattern: '*.pdf'
        publishDir 'passBams', mode: 'copy', overwrite: true, pattern: '*passed.bam'
        publishDir 'passBams', mode: 'copy', overwrite: true, pattern: '*passed.bam.bai'

	input:
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file("${bc}.blast") from rblast

	output:
        file("*.pdf") optional true into loxPmixs
	tuple val(r),val(bc),val(exp), file('ref'), val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file("*passed.bam"), file("*passed.bam.bai") optional true into loxPmixBams
	
	script:
	d=file(params.determinantsCsv)
	"""
	crisont.py -b "${bc}.blast" -s \
			"${exp}_${bc}_${r}_q${q}.sorted.bam" \
			-r ref -rb ${r}.blast \
			-d $d \
			--barcode ${bc}

	"""

}


process passed_varcall {
	errorStrategy 'ignore'
	tag { bc + '_' + r  + '_' + rd}
	//conda 'bioconda::medaka samtools whatshap', pattern: 'medaka*'

	publishDir 'passBam_varcall' , mode: 'copy', overwrite: true, pattern: 'medaka*'
	

	input: 
	tuple val(r),val(bc),val(exp), file('ref'), val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file("*passed.bam"), file("*passed.bam.bai") from loxPmixBams
	
	output:
	file("*medaka*") into passed_med

	script:
	rd=params.vc_covs
	"""
	cov_reduction.py passed.bam reduction.bam $rd ${exp}_${bc}_${r}_${q}.depth.tsv
	medaka_variant -i reduction.bam -f ref -o medaka_${bc}_${r} -m ${med_model} -s ${med_model} -t 4
	bedtools intersect -a medaka_${bc}_${r}/round_1.vcf -b ${r}_${bc}_filt.bed -v -header > medaka_${r}_${bc}_filt_passBam.vcf
	
	"""

}

}


if (params.strategy == 'medaka_whatshap') {

process medaka {
	errorStrategy 'ignore'
	tag { bc + '_' + r  + '_' + rd}
	//conda 'bioconda::medaka samtools whatshap', pattern: 'medaka*'

	publishDir 'First_pass_varcall' , mode: 'copy', overwrite: true, pattern: 'medaka*'
	

	input: 
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed") from bed_varcall
	
	
	output:
	tuple val(bc), val(r), val(exp), file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"),file('ref'),val(q), file("medaka_${bc}_${r}"), file("medaka_${r}_${bc}_filt.vcf"), file("${r}_${bc}_filt.bed")  into vcffilt
       	file("${exp}_${bc}_${r}_${q}.depth.tsv") into depths2

	script:
	rd=params.vc_covs
	"""
	cov_reduction.py ${exp}_${bc}_${r}_q${q}.sorted.bam reduction.bam $rd ${exp}_${bc}_${r}_${q}.depth.tsv
	medaka_variant -i reduction.bam -f ref -o medaka_${bc}_${r} -m ${med_model} -s ${med_model} -t 4
	bedtools intersect -a medaka_${bc}_${r}/round_1.vcf -b ${r}_${bc}_filt.bed -v -header > medaka_${r}_${bc}_filt.vcf
	"""

}



process haplotag {
	errorStrategy 'ignore'
	tag { bc + '_' + r }
	//conda 'bioconda:: samtools whatshap'

	publishDir 'haplotagged' , mode: 'copy', overwrite: true, pattern: '*.hap*'

	input:
	tuple val(bc), val(r), val(exp), file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"),file('ref'),val(q), file("medaka_${bc}_${r}"), file("medaka_${r}_${bc}_filt.vcf"), file("${r}_${bc}_filt.bed")  from vcffilt

	output:
	tuple val(bc), val(r), val(exp), file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"),file('ref'),val(q), file("medaka_${r}_${bc}_filt.vcf"), file("${exp}_${bc}_${r}_q${q}.haplotagged.bam"), file("${exp}_${bc}_${r}_q${q}.haplotagged.bam.bai"), file("${r}_${bc}_filt.bed") into tagged

	script:
	"""
	samtools faidx ref
	bgzip -c medaka_${r}_${bc}_filt.vcf > medaka_${r}_${bc}_filt.vcf.gz
	tabix -p vcf medaka_${r}_${bc}_filt.vcf.gz
	whatshap haplotag medaka_${r}_${bc}_filt.vcf.gz ${exp}_${bc}_${r}_q${q}.sorted.bam --reference $ref --ignore-read-groups -o ${exp}_${bc}_${r}_q${q}.haplotagged.bam
	samtools index ${exp}_${bc}_${r}_q${q}.haplotagged.bam
	"""

}

process hapsplit {
	errorStrategy 'ignore'
	tag { bc + '_' + r }
	//conda 'bioconda::medaka samtools whatshap pysam'

	publishDir 'hapsplit' , mode: 'copy', overwrite: true, pattern: '*hap2*'
	publishDir 'hapsplit' , mode: 'copy', overwrite: true, pattern: '*hap1*'
	publishDir 'hapsplit' , mode: 'copy', overwrite: true, pattern: '*hapmisc*'

	input:
	tuple val(bc), val(r), val(exp), file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"),file('ref'),val(q), file("medaka_${r}_${bc}_filt.vcf"), file("${exp}_${bc}_${r}_q${q}.haplotagged.bam"), file("${exp}_${bc}_${r}_q${q}.haplotagged.bam.bai"), file("${r}_${bc}_filt.bed") from tagged
	file("${exp}_${bc}_${r}_${q}.depth.tsv") from depths2
	

	output:
	tuple val(bc), val(r), val(exp), file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"),file('ref'),val(q), file("medaka_${r}_${bc}_filt.vcf"), file("${exp}_${bc}_${r}_q${q}.haplotagged.bam"), file("${exp}_${bc}_${r}_q${q}.haplotagged.bam.bai"), file("medaka_${bc}_hap1"), file("medaka_${bc}_hap2"), file("${r}_${bc}_filt.bed") into hp
	tuple val(bc), val(r), val(exp), file('ref'), val(q), file("${exp}_${bc}_${r}_q${q}.hap1.bam"), file("${exp}_${bc}_${r}_q${q}.hap2.bam"), file("${exp}_${bc}_${r}_q${q}.hap1.bam.bai"), file("${exp}_${bc}_${r}_q${q}.hap2.bam.bai") into sniffles 

	

	script:
	rd=params.vc_covs
	"""
	hapsplit.py ${exp}_${bc}_${r}_q${q}.haplotagged.bam ${exp}_${bc}_${r}_q${q}.hap1.bam ${exp}_${bc}_${r}_q${q}.hap2.bam ${exp}_${bc}_${r}_q${q}.hapmisc.bam
	
	samtools index ${exp}_${bc}_${r}_q${q}.hap1.bam
	samtools index ${exp}_${bc}_${r}_q${q}.hap2.bam
	samtools index ${exp}_${bc}_${r}_q${q}.hapmisc.bam
	

	cov_reduction.py ${exp}_${bc}_${r}_q${q}.hap1.bam reduction_hap1.bam $rd ${exp}_${bc}_${r}_${q}.depth.tsv
	cov_reduction.py ${exp}_${bc}_${r}_q${q}.hap2.bam reduction_hap2.bam $rd ${exp}_${bc}_${r}_${q}.depth.tsv
	cov_reduction.py ${exp}_${bc}_${r}_q${q}.hapmisc.bam reduction_hapmisc.bam $rd ${exp}_${bc}_${r}_${q}.depth.tsv
	medaka_variant -i reduction_hap1.bam -f ref -o medaka_${bc}_hap1 -s ${med_model} -S -t 4
	medaka_variant -i reduction_hap2.bam -f ref -o medaka_${bc}_hap2 -s ${med_model} -S -t 4
	medaka_variant -i reduction_hapmisc.bam -f ref -o medaka_${bc}_hapmisc -s ${med_model} -S -t 4
	"""
}

process vcf_filt_haps {
	errorStrategy 'ignore'
	tag { bc + '_' + r }
	//conda 'bioconda:: blast bedops bedtools'

	publishDir 'Haplotyped_vcfs' , mode: 'copy', overwrite: true, pattern: '*.vcf'

	
	input: 
	tuple val(bc), val(r), val(exp), file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"),file('ref'),val(q), file("medaka_${r}_${bc}_filt.vcf"), file("${exp}_${bc}_${r}_q${q}.haplotagged.bam"), file("${exp}_${bc}_${r}_q${q}.haplotagged.bam.bai"), file("medaka_${bc}_hap1"), file("medaka_${bc}_hap2"), file("${r}_${bc}_filt.bed") from hp
	

	output:
	tuple val(bc), val(r), val(exp), file("medaka_${r}_${bc}_hap1_filt.vcf"), file("medaka_${r}_${bc}_hap2_filt.vcf") into fin
	
	
	script:
	"""
	bedtools intersect -a medaka_${bc}_hap1/round_1.vcf -b ${r}_${bc}_filt.bed -v -header > medaka_${r}_${bc}_hap1_filt.vcf
	bedtools intersect -a medaka_${bc}_hap2/round_1.vcf -b ${r}_${bc}_filt.bed -v -header > medaka_${r}_${bc}_hap2_filt.vcf	
	"""

}
	
}





	


	


